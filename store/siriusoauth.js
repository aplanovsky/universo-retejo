import Vue from 'vue'

export default {
  namespaced: true,
  state () {
    return {
      user: null,
      loggedIn: null,
      konfirmita: null
    }
  },
  getters: {
    user: (state) => {
      return state.user
    },
    isLoggedIn: (state) => {
      return state.loggedIn
    },
    konfirmita: (state) => {
      return state.konfirmita
    }
  },
  mutations: {
    setUserData(state, payload) {
      Vue.set(state, "user", payload)
    },
    setLoginData(state, payload) {
      state.loggedIn = payload
    },
    setKonfirmData(state, payload) {
      state.konfirmita = payload
    },
    setToken (state, token) {
      state.token = token
    },
    errors (state, error) {
      state.errors.push(error)
    }
  },
  actions: {
    setUser: (store, payload) => store.commit("setUserData", payload),
    setIsLoggedIn: (store, payload) => store.commit("setLoginData", payload),
    setKonfirmita: (store, payload) => store.commit("setKonfirmData", payload)
  }
}
